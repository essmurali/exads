<?php
/*
This program generates the random integer between 1 and 500
and shuffles the array
and remove the last element from the shuffled array
Then find the difference between the original and shuffled array
then print the difference value, which gives the removed element from an array
*/
$rangeVal = $originalArr = range(1,500);
shuffle($rangeVal);
array_pop($rangeVal);
$truncatedArr = $rangeVal;
$result=array_diff($originalArr,$truncatedArr);
echo implode($result, ',');
?>