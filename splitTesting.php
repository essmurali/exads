<?php
/*
Get the random integer between 2 and 5, which gives the propbability of even number twice 
so redirects to design1.php 50%
other pages will be called only once so percent will be 25%
*/
srand((double)microtime()*1000000);
$randVal = rand(2,5);
if($randVal % 2 == 0)
	header("Location: design1.php");
elseif($randVal == 3)
	header("Location: design2.php");
elseif($randVal == 5)
	header("Location: design3.php");
?>