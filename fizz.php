<?php
$rangeVal = range(1,100);
foreach($rangeVal as $val)
{
	if($val%3 == 0 && $val%5 == 0)
	{
		echo "FizzBuzz<br>";
	}
	elseif($val%3 == 0)
	{
		echo "Fizz<br>";
	}
	elseif($val%5 == 0)
	{
		echo "Buzz<br>";
	}
	else
	{
		echo $val."<br>";
	}
}
?>