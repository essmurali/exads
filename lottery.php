<?php
/*
Fetch the next draw date & time based on the passed datetime or by default current datetime
*/
echo getNextDrawTime();

function getNextDrawTime($refdate = "")
{
	if(trim($refdate) == "")
		$refdate = date("Y-m-d")." 20:00:00";
	$timestamp = date("Y-m-d", strtotime($refdate)); 
	$timestamp = strtotime($timestamp." 20:00:00");
	$dayOfWeek = date('w', $timestamp);
	if($dayOfWeek >= 3)
	{

		if(strtotime("now") < $timestamp && date("w") == "3")
			return date('Y-m-d H:i:s', strtotime("now" . date('H:i:s', $timestamp), $timestamp));
		else
			return date('Y-m-d H:i:s', strtotime("next saturday " . date('H:i:s', $timestamp), $timestamp));
	}
	else
	{
		if(strtotime("now") < $timestamp && date("w") == "6")
			return date('Y-m-d H:i:s', strtotime("now" . date('H:i:s', $timestamp), $timestamp));
		else
	 		return date('Y-m-d H:i:s', strtotime("next wednesday " . date('H:i:s', $timestamp), $timestamp));
	}
}
?>