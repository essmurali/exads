<?php
$host = "localhost";
$user = "root";
$pass = "root";
$conn = mysqli_connect($host, $user, $pass, "exads");

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
$sql = "SELECT name, age, job_title FROM exads_test";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo " - Name: " . $row["name"]. ",  Age: " . $row["age"]. ", Job Title: ".$row['job_title']."<br>";
    }
} else {
    echo "0 results";
}
//Prepared sql query to insert the sanitized record into the table

$stmt = $conn->prepare("INSERT INTO exads_test (name, age, job_title) VALUES (?, ?, ?)");
$stmt->bind_param("sds", $name, $age, $job_title);

// set parameters and execute
$name = "Murali";
$age = "32";
$job_title = "PHP Programmer";
$stmt->execute();
$conn->close();
?>